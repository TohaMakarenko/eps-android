package lab1a.lab1a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Optional;

public class NeuronActivity extends AppCompatActivity {

    protected double sigma = 0.1;
    protected Dot x1;
    protected Dot x2;
    protected double p = 5;
    protected double initial_w1 = 0;
    protected double initial_w2 = 0;

    EditText sigmaEdit;
    EditText x1_1Edit;
    EditText x1_2Edit;
    EditText x2_1Edit;
    EditText x2_2Edit;
    EditText pEdit;
    EditText w1Edit;
    EditText w2Edit;
    EditText resEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neuron);

        sigmaEdit = findViewById(R.id.sigma);
        x1_1Edit = findViewById(R.id.x1_1);
        x1_2Edit = findViewById(R.id.x1_2);
        x2_1Edit = findViewById(R.id.x2_1);
        x2_2Edit = findViewById(R.id.x2_2);
        pEdit = findViewById(R.id.p_e);
        w1Edit = findViewById(R.id.w1);
        w2Edit = findViewById(R.id.w2);
        resEdit = findViewById(R.id.resEdit);

        Button runMain = findViewById(R.id.RunFactor);

        runMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NeuronActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


        Button runLearning = findViewById(R.id.runLearning);

        runLearning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetAllParams();
                Result res = new NeuralNetwork().run(x1, x2, p, sigma);
                resEdit.setText(String.format("%.3f", res.w1)+ "; " + String.format("%.3f", res.w2));
            }
        });
    }

    protected void GetAllParams() {
        sigma = GetDouble(sigmaEdit);
        x1 = new Dot(GetDouble(x1_1Edit), GetDouble(x1_2Edit));
        x2 = new Dot(GetDouble(x2_1Edit), GetDouble(x2_2Edit));
        p = GetDouble(pEdit);
        initial_w1 = GetDouble(w1Edit);
        initial_w2 = GetDouble(w2Edit);
    }

    protected double GetDouble(EditText et) {
        return Double.parseDouble(et.getText().toString());
    }
}

class NeuralNetwork {

    public Result run(Dot d1, Dot d2, final double p, final double sigma) {
        double y1 = 0, y2 = 0;
        double w1 = 0, w2 = 0;
        int counter = 0;
        while (!isFinish(y1, y2, p) && counter < 1000) {
            y1 = getY(d1, w1, w2);
            w1 = recalculateW(d1.x, w1, p - y1, sigma);
            w2 = recalculateW(d1.y, w2, p - y1, sigma);

            if (isFinish(y1, y2, p))
                break;

            y2 = getY(d2, w1, w2);
            w1 = recalculateW(d2.x, w1, p - y2, sigma);
            w2 = recalculateW(d2.y, w2, p - y2, sigma);
            counter++;
        }
        if (counter == 1000)
            return null;
        return new Result(w1, w2);
    }

    private boolean isFinish(double y1, double y2, double p) {
        return (y1 > p) && (y2 < p);
    }

    private double getY(Dot dot, double w1, double w2) {
        return w1 * dot.x + w2 * dot.y;
    }

    private double recalculateW(double x, double w, double delta, double sigma) {
        return w + delta * x * sigma;
    }
}

class Dot {
    public Dot(double x, double y) {
        this.x = x;
        this.y = y;
    }

    double x;
    double y;
}


class Result {
    public Result(double w1, double w2) {
        this.w1 = w1;
        this.w2 = w2;
    }

     double w1;
     double w2;

    public double getW1() {
        return w1;
    }

    public void setW1(double w1) {
        this.w1 = w1;
    }

    public double getW2() {
        return w2;
    }

    public void setW2(double w2) {
        this.w2 = w2;
    }
}