package lab1a.lab1a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.Math.pow;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button runNeuron = findViewById(R.id.RunNeuron);

        runNeuron.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NeuronActivity.class);
                startActivity(intent);
            }
        });

        Button calculateButton = findViewById(R.id.calculate);

        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText numberText = findViewById(R.id.number);
                String numberStr = numberText.getText().toString();
                int number;
                try {
                    number = Integer.parseInt(numberStr);
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "Enter number", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (number < 3) {
                    Toast.makeText(MainActivity.this, "Enter number >2", Toast.LENGTH_SHORT).show();
                    return;
                }
                long startTime = System.nanoTime();
                int a = number + 1;
                int b = 3;
                int count = 0;
                boolean flag = false;
                long endTime = 0;
                while (count < 100 && !flag) {
                    while (b < a && !flag) {
                        if ((pow(a, 2) - pow(number, 2)) == pow(b, 2)) {
                            endTime = System.nanoTime();
                            flag = true;
                            break;
                        }
                        b++;
                    }
                    if(flag)
                        break;
                    a++;
                    b = 3;
                    count++;
                }

                if(flag) {
                    TextView aView = findViewById(R.id.a);
                    aView.setText(String.valueOf(a));
                    TextView bView = findViewById(R.id.b);
                    bView.setText(String.valueOf(b));
                    TextView timeView = findViewById(R.id.time);
                    timeView.setText(String.valueOf((endTime - startTime)));
                }
                if(!flag)
                    Toast.makeText(MainActivity.this, "Cannot find a and b", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
